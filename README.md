# Node.js, 3 days
### 12 - 14 September 2022

Welcome to this course. Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

_N.B._ Solutions and Demos programs will be pushed during the course.


# Links
* [Installation instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/javascript/nodejs/)



Course GIT Repo
====
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a sub-directory for
each chapter. Get the course repo initially by a `git clone` operation

![Git Clone](img/git-clone.png)

    mkdir -p ~/nodejs-course/my-solutions
    cd ~/nodejs-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/nodejs-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
