const { reverseSentence } = require('./reverse.js');

test('simple sentence', () => {
    expect(reverseSentence('hej Hopp hIPP'))
        .toBe('hIPP Hopp hej');
})

test('one word', () => {
    expect(reverseSentence('hejsan'))
        .toBe('hejsan');
})

test('empty string', () => {
    expect(reverseSentence(''))
        .toBe('');
})

test('space string', () => {
    expect(reverseSentence('        '))
        .toBe('');
})

test('null argument', () => {
    expect(() => { reverseSentence(null); })
        .toThrow();
})

test('undefined argument', () => {
    let value;
    expect(() => { reverseSentence(value); })
        .toThrow();
})


