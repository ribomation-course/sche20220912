
function reverseSentence(sentence) {
    if (sentence === null || sentence === undefined) throw new Error('no data');
    if (/^\s*$/.test(sentence)) return '';
    return sentence.split(/\s+/).reverse().join(' ');
}

module.exports = {reverseSentence};
