import fetch from 'node-fetch';

const baseUrl = 'http://localhost:3000/products';

async function all() {
    const res = await fetch(baseUrl);
    if (res.ok) {
        return await res.json();
    }
}

async function one(id) {
    const res = await fetch(`${baseUrl}/${id}`);
    if (res.ok) {
        return await res.json();
    } else {
        return {};
    }
}

async function create(data) {
    const opts = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const res = await fetch(baseUrl, opts);
    if (res.ok) {
        return res.json();
    } else {
        throw new Error(`${res.status} ${res.statusText}`);
    }
}

async function remove(id) {
    const opts = { method: 'DELETE' };
    const res = await fetch(`${baseUrl}/${id}`, opts);
    if (res.ok) {
        return Promise.resolve({});
    } else {
        throw new Error(`${res.status} ${res.statusText}`);
    }
}


console.log('-- all --\n', await all());
console.log('-- one --\n', await one(1));
console.log('-- one 404 --\n', await one(1234));
const newObj = await create({ name: 'Potato', price: '€12,34', country: 'Sweden' })
console.log('-- create --\n', newObj);
console.log('-- delete --\n', await remove(newObj.id));
try {
    await one(newObj.id)
} catch(err) {
    console.log('removed id=%d', newObj.id);
}
