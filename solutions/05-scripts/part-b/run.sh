#!/usr/bin/env bash

npm install --global local-web-server

ws --directory www --port 8888 --open 

npm uninstall --global local-web-server
