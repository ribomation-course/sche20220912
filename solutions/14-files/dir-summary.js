const { readdirSync, statSync } = require('fs');
const { join } = require('path');

const dir   = process.argv[2] || __dirname;
const count = list(dir);
console.log('count: %o', count);

function list(dir) {
    if (!statSync(dir).isDirectory) {
        console.error('not a directory: %s', dir);
        process.exit(1);
    }

    let count = { bytes:0, files:0, dirs: 0 };
    readdirSync(dir).forEach(name => {
        const path = join(dir, name);
        const meta = statSync(path);

        if (meta.isFile() && !name.startsWith('.')) {
            count.files++;
            count.bytes += meta.size;
        } else if (meta.isDirectory()) {
            count.dirs++;
            const result = list(path);
            count.bytes += result.bytes;
            count.files += result.files;
            count.dirs += result.dirs;
        }
    });

    return count;
}

