let _factor = 100;

export function factor(value) {
    if (value) _factor = value;
    return _factor;
}

export function multiply(n) {
    return n * _factor;
}
