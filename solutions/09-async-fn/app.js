import { load, store } from './io-lib.js';

const srcfile = process.argv[2] || './app.js';
const destfile = srcfile + '.COPY';

async function copyfile(fromFile, toFile) {
    try {
        const payload = await load(fromFile);
        console.info('loaded %s (%d bytes)', fromFile, payload.length);

        await store(toFile, payload.toUpperCase());
        console.info('written %s', toFile);
    } catch (err) {
        console.error('failed: %o', err);
    } finally {
        console.debug('-- done --');
    }
}

copyfile(srcfile, destfile);
