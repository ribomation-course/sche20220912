import { readFile, writeFile } from 'node:fs';

export function load(path) {
    return new Promise((success, failure) => {
        readFile(path, 'utf8', (err, data) => {
            if (err) failure(err.message);
            else success(data);
        });
    });
}

export function store(path, content) {
    return new Promise((success, failure) => {
        writeFile(path, content, (err, data) => {
            if (err) failure(err.message);
            else success(data);
        });
    });
}

