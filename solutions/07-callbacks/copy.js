const fs       = require('fs');
const filename = process.argv[2] || __filename;

fs.readFile(filename, 'utf8', (err, content) => {
    if (err) { throw new Error(err.message); }

    const result  = content.toUpperCase();
    const outfile = filename + '.COPY';
    fs.writeFile(outfile, result, 'utf8', (err) => {
        if (err) { throw new Error(err.message); }

        console.info('written', outfile);
    });
});
