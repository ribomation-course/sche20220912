const clock = (id) => {
    const tab = (' '.repeat(15)).repeat(id-1);
    let cnt = 1;
    return () => {
        const time = new Date().toLocaleTimeString();
        console.info('%s [%d:%d] %s', tab, id, cnt++, time);
    }
};

setInterval(clock(1), 500);
setInterval(clock(2), 1000);
setInterval(clock(3), 2000);

setTimeout(() => {
    process.exit(0);
}, 10000);
