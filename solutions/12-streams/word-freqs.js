const { Transform } = require('stream');

const maxWords = +(process.argv[2]) || 25;
const minSize = +(process.argv[3]) || 5;

class WordExtracter extends Transform {
    constructor() {
        super();
    }
    _transform(chunk, enc, next) {
        const words = chunk.toString().split(/[^a-z]+/ig);
        words.forEach(word => this.push(word.toLowerCase()));
        next();
    }
}

class FreqAggregator extends Transform {
    constructor() {
        super();
        this.freqs = new Map();
    }

    _transform(word, enc, next) {
        //console.debug('word: [%s]', word);
        if (word.length >= minSize) {
            word = word.toString();
            const n = +(this.freqs.get(word)) || 0;
            this.freqs.set(word, n + 1);
        }
        next();
    }

    _final(next) {
        //console.debug('freqs: [%o]', this.freqs);
        [...this.freqs.entries()]
            .sort((lhs, rhs) => (rhs[1] - lhs[1]))
            .slice(0, maxWords)
            .map(pair => `${pair[0]}: ${pair[1]}\n`)
            .forEach(str => this.push(str))
            ;
        next();
    }
}

//usage: cat ../../files/musketeers.txt | node word-freqs.js
process.stdin
    .pipe(new WordExtracter())
    .pipe(new FreqAggregator())
    .pipe(process.stdout)
    ;

