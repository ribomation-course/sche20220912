const { Readable } = require('stream');

class FibonacciStream extends Readable {
    constructor(max) {
        super();
        this.max = max;
        this.f0 = 0n;
        this.f1 = 1n;
        this.cnt = 0;
    }
    _read(sz) {
        const payload = Buffer.from(String(this.f1) + '\n', 'ascii');
        this.push(payload);
        if (++this.cnt >= this.max) this.push(null);
        
        [this.f1, this.f0] = [this.f0 + this.f1,  this.f1];
    }
}

const N = +process.argv[2] || 10;
new FibonacciStream(N).pipe(process.stdout);

