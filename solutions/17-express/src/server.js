import { fileURLToPath } from 'node:url';
import { dirname } from 'node:path';
import express from 'express';
import resource from './person-dao.js';

const app = express();
const port = 3000;
const dir = dirname(fileURLToPath(import.meta.url));


app.use((req, res, next) => {
    console.info('REQ: %s %s', req.method, req.originalUrl);
    next();
});
app.use(express.json());
app.use(express.static(dir + '/assets'));


app.route(resource.URI)
    .get((req, res) => {
        res.json(resource.all());
    })
    .post((req, res) => {
        res.status(201).json(resource.create(req.body));
    });

app.route(resource.URI + '/:id')
    .all((req, res, next) => {
        const [index, object] = resource.lookup(req.params.id);
        if (index === -1) {
            res.sendStatus(404);
        } else {
            req.resourceIndex = index;
            req.resourceObject = object;
            next();
        }
    })
    .get((req, res) => {
        res.json(req.resourceObject);
    })
    .put((req, res) => {
        res.json(resource.update(req.resourceObject, req.body));
    })
    .delete((req, res) => {
        resource.remove(req.resourceIndex);
        res.sendStatus(204);
    });

app.listen(port, () => {
    resource.initDB();
    console.info('started: http://localhost:%d%s', port, resource.URI);
});
