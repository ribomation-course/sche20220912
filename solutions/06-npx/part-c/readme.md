# Create Project

    npm create vite@latest my-vue-app -- --template vue
    cd my-vue-app
    npm install

## Start Dev Server

    npm run dev

# Build the App

    npm run build

## Run the App 

    npm install --save-dev local-web-server
    npx ws --directory ./dist --open

