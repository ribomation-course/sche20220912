const { fork } = require('child_process');

const delay    = 1000;
const duration = 10 * delay;
const worker   = fork('./worker.js');

const itvId = setInterval(() => {
    const arg = genTxt();
    worker.send(arg);
}, delay);

setTimeout(() => {
    clearInterval(itvId);
    setImmediate(() => {
        worker.kill('SIGHUP');
    });
}, duration);

worker.on('message', (reply) => {
    console.log('[parent] %s', reply);
});

worker.on('close', (code) => {
    console.log('[parent] child closed: code=%d', code);
});
worker.on('exit', (code) => {
    console.log('[parent] child exited: code=%d', code);
});
worker.on('error', (code) => {
    console.log('[parent] child failed: code=%d', code);
});

const words = [
    'tjabba', 'habba', 'babba', 
    'foobar', 'hello', 'howdy'
];
let wordIdx = 0;

function genTxt() {
    return words[wordIdx++ % words.length]
}
