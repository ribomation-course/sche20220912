import { load, store } from './io-lib.js';

const srcfile  = process.argv[2] || './app.js';
const destfile = srcfile + '.COPY';

load(srcfile)
    .then(data => {
        console.info('loaded %s (%d bytes)', srcfile, data.length);
        return store(destfile, data.toUpperCase());
    })
    .then(_ => {
        console.info('written %s', destfile);
    })
    .catch(err => {
        console.error('failed %o', err);
    })
    .finally(() => {
        console.debug('-- done --');
    });
