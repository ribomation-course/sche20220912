let _factor = 10;

function factor(value) {
    if (value) _factor = value;
    return _factor;
}

function multiply(n) {
    return n * _factor;
}

module.exports = {factor, multiply};
