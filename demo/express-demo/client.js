const REQ     = require('request-promise-native');
const baseUrl = 'http://localhost:3000/persons';
const opts    = {json: true, uri: baseUrl};

(async () => {
    console.info('RES: %o', await REQ(opts));

    console.info('RES: %o', await REQ({
        ...opts, uri: baseUrl + '/' + 2
    }));

    let obj;
    console.info('RES: %o', obj = await REQ({
        ...opts,
        method: 'POST',
        body: {name: 'Nisse Hult', age: 42}
    }));
    console.info('ID=%d', obj.id);

    console.info('RES: %o', obj = await REQ({
        ...opts,
        uri: baseUrl + '/' + obj.id,
        method: 'PUT',
        body: {name: 'Per Silja'}
    }));

    console.info('RES: %o', await REQ(opts));
    console.info('RES: %o', await REQ({
        uri: baseUrl + '/' + obj.id,
        method: 'DELETE'
    }));
    console.info('RES: %o', await REQ(opts));
})();


