window.addEventListener('load', async () => {
    const reply     = await fetch('http://localhost:3000/persons');
    const objs      = await reply.json();
    const tbody     = document.querySelector('tbody');
    tbody.innerHTML = '';
    objs.forEach(obj => {
        const tr     = document.createElement('tr');
        tr.innerHTML = `
            <td> ${obj.id} </td> <td> ${obj.name} </td> <td> ${obj.age} </td>
        `;
        tbody.appendChild(tr);
    });
});


