import { mkDigit, mkNumber } from "../src/pattern";

expect.extend({
    toBetween(value, lb, ub) {
        const pass = (lb <= value) && (value <= ub);
        if (pass) {
            return {
                message: () =>
                    `expected ${value} not to be within range [${lb}, ${ub}]`,
                pass: true,
            };
        } else {
            return {
                message: () =>
                    `expected ${value} to be within range [${lb}, ${ub}]`,
                pass: false,
            };
        }
    },
});

test('digit', () => {
    const N = 1000;
    for (let n=0; n<N; ++n) 
        expect(mkDigit()).toBetween('0', '9')
});

test('simple number pattern', () => {
    expect(mkNumber('####'))
        .toMatch(/^\d{4,4}$/);
});

test('pattern', () => {
    expect(mkNumber('+46(0)###-####-##'))
        .toMatch(/^\+\d{2,3}\(0\)\d{3,3}-\d{4,4}-\d{2,2}$/)
});


