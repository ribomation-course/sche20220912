const FS        = require('fs');
const PATH      = require('path');
const PROMISIFY = require('util').promisify;

const open  = PROMISIFY(FS.open);
const fstat = PROMISIFY(FS.fstat);
const read  = PROMISIFY(FS.read);
const write = PROMISIFY(FS.write);
const close = PROMISIFY(FS.close);

function copyFile(fromFile, toFile) {
    let vars = {};
    open(fromFile, 'r')
        .then(fd => {
            vars.fd = fd;
            return fstat(fd);
        })
        .then(stat => {
            return Buffer.alloc(stat.size);
        })
        .then(buf => {
            return read(vars.fd, buf, 0, buf.length, null);
        })
        .then(data => {
            vars.content = data.buffer.toString();
            console.log('read', data.bytesRead, 'bytes from', fromFile);
            return close(vars.fd);
        })
        .then(() => {
            return open(toFile, 'w');
        })
        .then(fd => {
            vars.fd = fd;
            return write(fd, vars.content.toUpperCase());
        })
        .then(data => {
            console.log('written', data.bytesWritten, 'bytes to', toFile);
            return close(vars.fd);
        })
        .then(() => {
            console.log('done');
        })
        .catch(err => {
            console.error(err);
        });
}

const filename = process.argv[2] || PATH.basename(__filename);
copyFile(filename, 'xx-' + PATH.basename(filename, '.js') + '.txt');

