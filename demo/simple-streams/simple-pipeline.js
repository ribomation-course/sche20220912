const { Transform } = require('stream');

const csvTrans = new Transform({
    readableObjectMode: true,
    transform(buf, enc, next) {
        const payload = buf.toString().trim();
        console.debug('csv',payload);
        this.push(payload.split(','));
        next();
    }
});

const arrTrans = new Transform({
    readableObjectMode: true,
    writableObjectMode: true,
    transform(arr, enc, next) {
        console.debug('arr',arr);
        const obj = {};
        for (let k = 0; k < arr.length; k += 2) {
            const key = arr[k];
            const val = arr[k + 1];
            //console.debug('arr',key,val);
            obj[key] = val;
        }
        this.push(obj);
        //console.debug('arr',obj);
        next();
    }
});

const objTrans = new Transform({
    writableObjectMode: true,
    transform(obj, enc, next) {
        console.debug('obj',obj);
        const json = JSON.stringify(obj, null, 2);
        this.push(json + '\n');
        next();
    }
});


process.stdin
    .pipe(csvTrans)
    .pipe(arrTrans)
    .pipe(objTrans)
    .pipe(process.stdout);
