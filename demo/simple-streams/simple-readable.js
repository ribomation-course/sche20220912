const {Readable} = require('stream');

let nextCode = 33;
const lastCode = 126;
const inStream = new Readable({
    read(numBytes) {
        const chr = String.fromCharCode(nextCode);
        this.push(chr);
        if (nextCode++ >= lastCode) {
            this.push('\n');
            this.push(null);
        }
    }
});

inStream.pipe(process.stdout);
