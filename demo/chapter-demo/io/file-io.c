#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main() {
    const char* txt = "This is a silly message, from C\n";
    
    int fd = open("silly.txt", O_WRONLY | O_CREAT | O_TRUNC);
    if (fd == -1) {
        printf("error: %s (%d)\n", strerror(errno), errno); exit(1);
    }

    int cnt = write(fd, txt, strlen(txt));
    if (cnt == -1) {
        printf("error: %s (%d)\n", strerror(errno), errno); exit(1);
    }
    printf("written %d bytes\n", cnt);

    close(fd);
    return 0;
}


