const {openSync, closeSync, readSync, writeSync} = require('fs');

const filename = './silly-text.txt';
const payload = `1st line
just a silly text
and it goes on and on
on, last line here
`;

let cnt;
{
    const fd = openSync(filename, 'w');
    cnt      = writeSync(fd, payload);
    closeSync(fd);
    console.log(`written %d bytes, to %s`, cnt, filename);
}
{
    const fd  = openSync(filename, 'r');
    const buf = Buffer.alloc(cnt);
    cnt       = readSync(fd, buf, 0, cnt, 0);
    closeSync(fd);
    console.log(`read %d bytes, from %s`, cnt, filename);
    const same = (payload === buf.toString());
    console.log(`restored: %s`, same);
}

