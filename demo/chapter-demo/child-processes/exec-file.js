const {execFile} = require('child_process');

execFile('/bin/ls', ['-lhFA', '.'], (err, stdout, stderr) => {
    if (err) {
        console.error('err', err);
        throw new Error(err);
    }
    console.info('stdout:\n%s', stdout);
    console.warn('stderr:\n%s', stderr);
});


