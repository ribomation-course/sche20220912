const {exec} = require('child_process');

exec('wc $(find .. -name "*.js")', (err, stdout, stderr) => {
    if (err) {
        console.error('err', err);
        throw new Error(err);
    }
    console.info('stdout:\n%s', stdout);
    console.warn('stderr:\n%s', stderr);
});


