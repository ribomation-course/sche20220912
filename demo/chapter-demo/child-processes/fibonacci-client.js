const {fork} = require('child_process');

let k   = +process.argv[2] || 1;
const N = +process.argv[3] || 10;
const T = +process.argv[4] || 1000;

const worker = fork('./fibonacci-server');
worker.on('message', (reply) => {
    console.log('[parent] fib(%d) = %s', reply.argument, reply.result)
});

const id = setInterval(() => {
    worker.send(k++);
    if (k > N) {
        clearInterval(id);
        worker.send(0);
    }
}, T);

