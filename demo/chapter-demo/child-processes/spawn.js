const {spawn} = require('child_process');
const {createReadStream, createWriteStream, statSync} = require('fs');

const infile  = process.argv[2] || '../../musketeers.txt';
const outfile = 'words.txt';

const src   = createReadStream(infile);
const words = spawn('tr', ['-C', '[:alpha:]', '\n']);
const lower = spawn('tr', ['A-Z', 'a-z']);
const sort  = spawn('sort');
const uniq  = spawn('uniq', ['-c']);
const desc  = spawn('sort', ['-r']);
const dst   = createWriteStream(outfile);

src.pipe(words.stdin);
words.stdout.pipe(lower.stdin);
lower.stdout.pipe(sort.stdin);
sort.stdout.pipe(uniq.stdin);
uniq.stdout.pipe(desc.stdin);
desc.stdout.pipe(dst)
    .on('finish', () => {
        console.info('written %s, %d bytes',
            outfile, statSync(outfile).size);
    });


