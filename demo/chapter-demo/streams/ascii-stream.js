const { Readable } = require('stream');

class AsciiStream extends Readable {
    constructor(first = 48, last = 90) {
        super();
        this.nextCharCode = first;
        this.lastCharCode = last;
    }
    _read(numBytesToRead) { //invoked by the consumer
        const chr = String.fromCharCode(this.nextCharCode);
        this.push(chr);
        if (this.nextCharCode >= this.lastCharCode) {
            this.push('\n');
            this.push(null);
        }
        this.nextCharCode++;
    }
}

const src = new AsciiStream(32, 126);
src.pipe(process.stdout);


// src.on('data', (chunk) => {
//     console.log(`RECV: "${chunk.toString().replace(/\n/g, '<NL>')}"`);
// });
// src.on('end', () => {
//     console.log('--done--');
// });




