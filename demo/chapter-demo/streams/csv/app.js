const LineTransformer         = require('./line-transformer');
const SplitTransformer        = require('./split-transformer');
const Array2ObjectTransformer = require('./arr2object-transformer');
const JsonArrayTransformer    = require('./jsonarray-transformer');
const { PassThrough }         = require('stream');
const fs                      = require('fs');
const path                    = require('path');
const zlib                    = require('zlib');

const filename = process.argv[2] || './persons.csv';
const pretty   = (process.argv[3] || '') === 'pretty';

let outfile = path.basename(filename, '.csv') + '.json';
const ext   = path.extname(filename);
let compressed = false;
if (ext === '.gz') {
    compressed = true;
    outfile    = path.basename(path.basename(filename, '.gz'), '.csv') + '.json';
}

fs.createReadStream(filename)
    .pipe(compressed ? zlib.createGunzip() : new PassThrough())
    .pipe(new LineTransformer())
    .pipe(new SplitTransformer(';'))
    .pipe(new Array2ObjectTransformer())
    .pipe(new JsonArrayTransformer(pretty))
    .pipe(fs.createWriteStream(outfile))
    .on('finish', () => { console.info(filename, '-->', outfile); })
    ;

