const { Transform } = require('stream');
const NL = '\n'.charCodeAt(0);

class LineTransformer extends Transform {
    constructor() { super(); }
    _transform(buf, enc, next) {
        let start = 0;
        let end = buf.indexOf(NL, start);
        while (end >= 0) {
            const line = buf.slice(start, end);
            this.push(line);
            start = end + 1;
            end = buf.indexOf(NL, start);
        }
        next();
    }
}

module.exports = LineTransformer;

