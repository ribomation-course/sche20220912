const { Transform } = require('stream');

class SplitTransformer extends Transform {
    constructor(delim = ";") {
        super({
            readableObjectMode: true,
        });
        this.SEP = delim.charCodeAt(0);
    }
    _transform(buf, enc, next) {
        const fields = [];
        let start    = 0;
        let end      = buf.indexOf(this.SEP, start);
        while (end >= 0) {
            const field = buf.slice(start, end).toString().trim();
            if (field.length > 0) fields.push(field);
            start = end + 1;
            end   = buf.indexOf(this.SEP, start);
        }
        if (start < buf.length) {
            const field = buf.slice(start).toString().trim();
            if (field.length > 0) fields.push(field);
        }
        this.push(fields);
        next();
    }
}

module.exports = SplitTransformer;

