const { Writable } = require('stream');

class ListStream extends Writable {
    constructor() {
        super();
        this.storage = [];
    }
    _write(data, enc, next) {
        this.storage.push(data.toString());
        next();
    }
    _final(next) {
        console.debug('--done--');
        next();
    }
    get payload() { return this.storage; }
}

const dest = new ListStream();
dest.on('finish', () => {
    let cnt = 1;
    dest.payload.forEach((data) => {
        console.log(cnt++, ')',
            data.replace(/\s+/g, '').slice(0, 80),
            '...');
    });
});

process.stdin.pipe(dest);



