const http = require('http');
const port = 8000;
http
    .createServer((request, response) => {
        const op  = request.method;
        const url = request.url;
        console.debug('REQ: %s %s', op, url);

        const payload = 'Hello from a simple HTTP server\r\n';
        response.writeHead(200, {
            'Content-Type': 'text/plain',
            'Content-Length': payload.length,
            'Date': new Date().toLocaleTimeString(),
        });
        response.write(payload);
        response.end();
    })
    .listen(port, _ => {
        console.info('server started: http://localhost:%d/', port);
    });
