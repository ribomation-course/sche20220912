#!/usr/bin/env bash
set -eu
URL="http://localhost:8000/persons"

function getAll() {
  echo "-- READ All --"
  ( set -x; http $URL )
}

function get() {
  echo "-- READ One --"
  ( set -x; http "${URL}/$1" )
}

function create() {
  echo "-- CREATE --"
  ( set -x; http -v $URL name=Nisse age=53 )
}

function update() {
  echo "-- UPDATE --"
  ( set -x; http -v put "$URL/$1" name=$2 )
}

function delete() {
  echo "-- DELETE --"
  ( set -x; http -v delete "$URL/$1" )
}

getAll
get 1
create
getAll
update 3 'Olle'
get 3
delete 3
getAll


