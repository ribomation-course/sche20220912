const {request} = require('http');

const payload = JSON.stringify({
    name: 'Per Silja', age: 33
});
const config  = {
    host: 'localhost',
    port: 8000,
    method: 'POST',
    path: '/persons',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': payload.length
    }
};

const REQ = request(config, RES => {
    console.info('STATUS: %d', RES.statusCode);
    let body = [];
    RES.on('data', chunk => body.push(chunk))
        .on('end', () => {
            body = JSON.parse(Buffer.concat(body).toString());
            console.info('BODY: %o', body);
        })
        .on('error', ERR => console.error(ERR));
});

REQ.write(payload);
REQ.end();
REQ.on('error', ERR => console.error(ERR));

