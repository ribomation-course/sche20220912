function fib(n) {
    if (n <= 0) return 0;
    if (n === 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

console.time('fib');
let N = 42;
let F = fib(N);
console.log('fib(%d) = %d', N, F);
console.timeEnd('fib');
