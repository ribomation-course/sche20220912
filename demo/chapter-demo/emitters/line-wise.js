const fs = require('fs');
const rl = require('readline');

const filename = process.argv[2] || __filename;
const maxWords = process.argv[3] || 15;
const minSize  = process.argv[4] || 3;
const freqs    = new Map();
console.time('elapsed');

const input = rl.createInterface({
    input: fs.createReadStream(filename)
});

input.on('line', (line) => {
    line.split(/[^a-z]+/gi)
        .forEach(word => {
            if (word.length >= minSize) {
                word = word.toLowerCase();
                freqs.set(word, 1 + (freqs.get(word) || 0));
            }
        });
});

input.on('close', () => {
    const result = [...freqs.entries()]
        .sort((lhs, rhs) => (rhs[1] - lhs[1]))
        .slice(0, maxWords);

    const maxSize = result
        .map(pair => pair[0])
        .reduce((mx, w) => w.length > mx ? w.length : mx, 0);

    result.forEach((pair) => {
        console.log('%s: %d', pair[0].padEnd(maxSize), pair[1]);
    });
    console.log('----')
    console.timeEnd('elapsed');
});


