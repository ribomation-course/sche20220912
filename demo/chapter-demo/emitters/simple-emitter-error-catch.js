const EventEmitter = require('events');
class TinyEmitter extends EventEmitter { }

console.time('total');
const now = () => new Date().toISOString().substr(11,12);
const emmi = new TinyEmitter();
emmi.on('hello', () => {
    console.log(now(), 'event "hello"');
});
emmi.on('wassup', () => {
    console.log(now(), 'event "wassup"');
});
emmi.on('bye', () => {
    console.log(now(), 'event "bye"');
    console.timeEnd('total');
});
emmi.on('error', (err) => {
    console.log(now(), 'crap:', err.message);
});

let N = 1;
setTimeout(() => emmi.emit('hello') , 1000 * N++);
setTimeout(() => emmi.emit('wassup'), 1000 * N++);
setTimeout(() => emmi.emit('wassup'), 1000 * N++);
setTimeout(() => emmi.emit('wassup'), 1000 * N++);
setTimeout(() => emmi.emit('wassup'), 1000 * N++);
setTimeout(() => emmi.emit('bye')   , 1000 * N++);
setTimeout(() => emmi.emit('error', new Error('bugger')), 3500);

console.log(now(), 'started');


