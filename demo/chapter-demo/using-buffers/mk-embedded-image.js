const {readFileSync, writeFileSync} = require('fs');

const imgFile  = './smiley-100x72.png';
const htmlFile = './embedded-image.html';
const type     = 'image/png';
const encoding = 'base64';

const buf    = readFileSync(imgFile);
const prefix = `data:${type};${encoding},`;
const html   = `<html>
<body>
    <h1>Embedded Image</h1>
    <img src="${prefix + buf.toString(encoding)}" alt="Smiley" />
</body>
</html>`;

writeFileSync(htmlFile, html);
console.log('written %s', htmlFile);

