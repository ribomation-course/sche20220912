import fetch from "node-fetch";

const url = 'https://jsonplaceholder.typicode.com/users';
const id  = Number(process.argv[2] || '1')

try {
    const res = await fetch(url + '/' + id)
    if (res.ok) {
        const data = await res.json();
        console.log(data);
    } else {
        console.error(`HTTP failed: ${res.status} ${res.statusText}`);
    }
} catch (err) {
    console.error('failed to fetch: ' + err);
}

