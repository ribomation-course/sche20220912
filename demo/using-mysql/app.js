import express from "express";
import { engine as hbsEngine } from 'express-handlebars';
import { createConnection } from "mysql2/promise";

const port = 4000;
const dbConf = {
    host: '172.20.78.66',
    port: '33060',
    user: 'nodejs',
    password: 'nodejs',
    database: 'products',
};

const db = await createConnection(dbConf);
const web = express();

web.engine('.hbs', hbsEngine({
    extname: '.hbs',
    defaultLayout: 'main',
}));
web.set('view engine', '.hbs');
web.set('views', './views');

web.use(express.static('./assets'));


web.use((req, _, next) => {
    console.log('%s %s', req.method, req.originalUrl);
    next();
});


web.get('/', (req, res) => {
    res.render('index');
});

web.get('/products', async (req, res) => {
    const sql = 'SELECT * FROM products ORDER BY name';
    const [rows, meta] = await db.query(sql);
    res.render('products', { products: rows });
});

web.get('/product/:id', async (req, res) => {
    const id = req.params.id;
    const sql = 'SELECT * FROM products WHERE id = ?';
    const [rows, meta] = await db.query(sql, [id]);
    if (rows.length === 1) {
        res.render('product-view', { product: rows[0] });
    } else {
        res.sendStatus(404);
    }
});


web.use((req, res, next) => {
    res.status(404).send({ message: 'Not found', path: req.url })
});

web.listen(port, () => {
    console.log(`server running. http://localhost:${port}/`);
});


