#!/usr/bin/env bash
set -eu
source common-vars.sh

set -x
docker image pull lherrera/cowsay
docker image pull mysql:8

docker image build --tag $MYSQL --file ./mysql.df . 

# docker network create $NET
docker container create --name $DB --publish $PORT:3306 $MYSQL

docker run --rm lherrera/cowsay 'MySQL DB @ Docker created'
