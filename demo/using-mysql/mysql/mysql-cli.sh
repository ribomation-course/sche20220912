#!/usr/bin/env bash
set -eu
source common-vars.sh

mysql --host=$HOST --port=$PORT --user=$USER --password=$PWD $DB_NAME
