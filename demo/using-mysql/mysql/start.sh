#!/usr/bin/env bash
set -eu
source common-vars.sh

docker container start $DB

docker run --rm lherrera/cowsay 'MySQL Docker container started. Wait a minute before connect.'

