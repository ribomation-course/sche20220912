#!/usr/bin/env bash
set -u
source common-vars.sh

docker container rm  $DB
# docker network rm $NET
docker image rm  $MYSQL

docker run --rm lherrera/cowsay 'MySQL DB @ Docker is removed and gone.'
