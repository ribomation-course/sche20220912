#!/usr/bin/env bash
set -eu
source common-vars.sh

docker container stop $DB

docker run --rm lherrera/cowsay 'MySQL Docker container stopped.'
